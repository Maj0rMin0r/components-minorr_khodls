package khodls_minorr.ui;

import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import khodls_minorr.shapes.Command;
import khodls_minorr.shapes.CommandInvoker;
import khodls_minorr.shapes.CompositeShape;
import khodls_minorr.shapes.Shape;
import khodls_minorr.shapes.ShapeManager;

/**
 * @author hornick
 * implemented by @author minorr
 * ShapeListUI - implements the UI that displays a list of shapes in a collection
 * We implement observer patterns now!
 */
public class ShapeListUI extends JFrame implements java.util.Observer{

	private JList<String> jlContents;
	private String cmdCombine = "Combine";
	private String cmdDecompose = "Decompose";
	private String cmdUndo = "Undo";
	private String cmdRedo = "Redo";
	private ShapeManager shapeManager;
    
	/**
	 * Class constructor: creates the UI
	 */
	public ShapeListUI(ShapeManager givenManager) {		
		shapeManager = givenManager;
		EventHandler eventHandler = new EventHandler();

		//Window
		setTitle("SE2811 Shape List"); // initial title
		setSize(400, 400);
		setLocation(200, 200);
		setResizable(true);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setVisible(true);

		Container contentPane = getContentPane();

		BorderLayout bl = new BorderLayout(0,0);
		contentPane.setLayout(bl);
		//		contentPane.setBackground(Color.BLACK);

		//Panel that contains the add, cancel, and save buttons
		JPanel jp = new JPanel();
		jp.setLayout(new FlowLayout(FlowLayout.CENTER));
		jp.setBackground(new Color(113, 142, 169));

		//"Add" button
		JButton btnCombine = new JButton(cmdCombine);
		//btnAdd.setBounds(20, 400, 139, 50);
		btnCombine.addActionListener(eventHandler);
		jp.add(btnCombine);

		//"Remove" button
		JButton btnDecompose = new JButton(cmdDecompose);
		//btnAdd.setBounds(20, 400, 139, 50);
		btnDecompose.addActionListener(eventHandler);
		jp.add(btnDecompose);
		
		//Undo
		JButton undo = new JButton(cmdUndo);
		undo.addActionListener(eventHandler);
		jp.add(undo);
		
		//Redo
		JButton redo = new JButton(cmdRedo);
		redo.addActionListener(eventHandler);
		jp.add(redo);
		
		contentPane.add(jp, BorderLayout.SOUTH);

		jlContents = new JList<String>(new DefaultListModel<String>() );
		//		jlContents.setBackground(Color.BLACK);	
		//		jlContents.setForeground(Color.WHITE);	
		Font font = new Font("Arial", Font.PLAIN, 12 );
		jlContents.setFont(font);
		JScrollPane spPLContents = new JScrollPane(jlContents, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);	// CCT lab 6
		spPLContents.setBounds(20, 40, 458, 340);
		contentPane.add(spPLContents, BorderLayout.CENTER);

		//"Contents" label
		JLabel lblContents = new JLabel();
		lblContents.setBounds(20, 20, 458, 20);
		lblContents.setText("Shape contents:");
		contentPane.add(lblContents, BorderLayout.NORTH);

		contentPane.validate();
		contentPane.repaint();
	}

	/**
	 * Override of java.util.Observable's update() method.
	 * Regenerates the list of shapes contained in the ShapeManager's collection 
	 * 
	 * We will really just ignore the arguments here, they exist solely to maintain our @override
	 */
	@Override
	public void update(Observable o, Object arg) {
		DefaultListModel<String> list = (DefaultListModel<String>)jlContents.getModel();
		list.clear();

		for(Shape thing: shapeManager.getShapes()){//Add all elements from the shape collection to the list...
			list.addElement(thing.getName());//...as their names
		}
	}

	/**
	 * Gathers the selected shapes from the list and calls
	 * the appropriate ShapeManager methods to composite the selected shapes
	 * @author minorr
	 */
	private void handleCombine() {
		int[] selectedShapes = jlContents.getSelectedIndices();
		ArrayList<Shape> theseShapes = new ArrayList<Shape>();
		for(int i= 0; i < selectedShapes.length; i++){//For as many shapes as we collected...
			theseShapes.add(shapeManager.getShapes().get(selectedShapes[i]));//...add the corresponding shape into the list we pass back
		}
		CommandInvoker.getInstance().invokeCommand(new CombineCommand(theseShapes));
	}

	/**
	 * Gathers the selected shapes from the list and calls
	 * the appropriate ShapeManager methods to decompose the selected composites;
	 * ignores non-composite selections 
	 * @author minorr
	 */
	private void handleDecompose() {
		int[] selectedComposites = jlContents.getSelectedIndices();
		ArrayList<CompositeShape> selected = new ArrayList<CompositeShape>();
		for(int i = 0; i<selectedComposites.length; i++){
			if(shapeManager.getShapes().get(selectedComposites[i]).isComposite()){
				selected.add((CompositeShape) shapeManager.getShapes().get(selectedComposites[i]));
			}
		}
		CommandInvoker.getInstance().invokeCommand(new DecomposeCommand(selected));
	}
	
	/**
	 * 
	 */
	private void handleRedo(){
		CommandInvoker.getInstance().redo();
	}
	
	/**
	 * Calls invoker's undo method to undo whatever just happened
	 * @author minorr
	 */
	private void handleUndo(){
		CommandInvoker.getInstance().undo();
	}
	
	/**
	 * Handles combining things
	 * @author minorr
	 *
	 */
	private class CombineCommand implements Command{
		ArrayList<Shape> selected;
		CompositeShape composed;
		
		public CombineCommand(ArrayList<Shape> inputShapes){
			selected = inputShapes;
		}
		
		@Override
		public void execute() {
			composed = shapeManager.compose(selected);//Compose and store what we made
		}

		@Override
		public void unexecute() {
			shapeManager.decompose(composed);//The thing we made? Kill that
		}	
	}
	
	/**
	 * Handles decombining things
	 * @author minorr
	 *
	 */
	private class DecomposeCommand implements Command{
		ArrayList<CompositeShape> selected;
		ArrayList<CompositeShape> decomposed;
		
		public DecomposeCommand(ArrayList<CompositeShape> inputShapes){
			selected = inputShapes;
			decomposed = new ArrayList<CompositeShape>();
		}
		
		@Override
		public void execute() {
			for(Shape thisShape : selected){//For every item...
				decomposed.add(shapeManager.decompose(thisShape));//...break it down into all children
			}
		}

		@Override
		public void unexecute() {
			for(CompositeShape thisShape : decomposed){//For everything we decomposed
				shapeManager.compose(thisShape.getChild());//Recompose
			}
			decomposed.clear();//If we recomposed them all, there is nothing left has been decomposed
		}
	}
	
	/**
	 * ShapeHierarchyUI event handler
	 * @author hornick
	 */
	private class EventHandler implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			String cmd = event.getActionCommand();
			if( cmd.equals(cmdCombine)){
				handleCombine();
			}else if( cmd.equals(cmdDecompose)){
				handleDecompose();
			}else if(cmd.equals(cmdUndo)){
				handleUndo();
			}else if(cmd.equals(cmdRedo)){
				handleRedo();
			}
		}
	}
}