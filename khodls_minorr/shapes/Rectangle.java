package khodls_minorr.shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

/**
 * This class represents an Rectangular shape that can draw itself to a graphics
 * context
 * @author hornick
 * @version 1.0
 * @created 21-Jan-2014 6:13:48 PM
 */
public class Rectangle extends AbstractShape {
	
	/**
	 * Constructor for initializing the attributes of a Rectangle
	 * 
	 * @param name    identifier for this shape
	 * @param clr    pen color to use when drawing this shape
	 * @param p1    starting (upper left) coordinate for this shape
	 * @param p2    ending (lower right) coordinate for this shape
	 */
	public Rectangle(String name, Color clr, Point p1, Point p2, Color outlineColor){
        super(name, clr, p1, p2, outlineColor);
   
	}

	/**
	 * This method implements the Rectangle-specific version of the draw() method
	 * defined within the abstract Shape class
	 * 
	 * @param g    - the Graphics context to use for drawing
	 */
	public void draw(Graphics g){
		  // System.out.println("DEBUG: Rectangle.draw: We are trying to draw an rectangle.");
	        int x = (int) Math.min(start.getX(), end.getX());
	        int y = (int) Math.min(start.getY(), end.getY());
	        int w = (int) Math.abs(start.getX() - end.getX());
	        int h = (int) Math.abs(start.getY() - end.getY());
	        g.setColor(color);
	        g.fillRect(x, y, w, h);
	      
	}

	/**
	 * used to get the outline color if an outline is desired
	 */
	@Override
	public Color getOutlineColor() {
		return outlineColor;
	}
}