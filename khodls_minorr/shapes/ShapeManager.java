package khodls_minorr.shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * @author hornick, yoder
 * modified by @author minorr
 * @version 2.0, now Observable
 * @created 21-Jan-2014 6:13:49 PM
 */
public class ShapeManager extends Observable{
	/**
	 * Fill color for newly created shapes
	 */
	private Color currentFillColor = Color.BLUE;
	/**
	 * If true, newly-created shapes should be labeled
	 */
	private boolean currentLabelFlag = false;
	/**
	 * Outline color for newly-created shapes
	 */
	private Color currentOutlineColor = Color.YELLOW;
	/**
	 * If true, newly-created shapes should have an outline
	 */
	private boolean outlineEnabledFlag = false;
	/**
	 * Our collection of what is observing us
	 */
	private List<java.util.Observer> ourUIObservers;
	
	/**
	 * The currently active shape-creation parameters. Any new shapes created
	 * via a call to ShapeManager's addShape() method will use these parameters.
	 * These are the default values, but they may be modified by calling this
	 * ShapeManager's mutator methods.
	 */
	private Shape.ShapeType currentShape = Shape.ShapeType.Rectangle;
	/**
	 * The number automatically assigned to each new Shape created by the
	 * ShapeManager; automatically incremented with each new Shape created.
	 */
	private static int sequenceNumber = 0;
	/**
	 * Main shape collection managed by ShapeManager
	 */
	private List<Shape> shapes;


	/**
	 * Default constructor.
	 */
	public ShapeManager() {
		shapes = new ArrayList<Shape>();
		ourUIObservers = new ArrayList<java.util.Observer>();
	}

	/**
	 * Causes the ShapeManager to create a new shape based on the currently
	 * selected parameters, and add the new shape to it's collection.
	 * @param start
	 *            coordinates specifying where the new shape starts
	 * @param tempEnd
	 *            coordinates specifying where the new shape tempEnds
	 */
	public Shape createNewShape(Point start, Point end) {
		Point tempStart = (Point)start.clone();
		Point tempEnd = (Point)end.clone();
		System.out.println(shapes.size());
		if (currentShape == Shape.ShapeType.Rectangle){
			if(!outlineEnabledFlag && !currentLabelFlag){
				shapes.add(new Rectangle("R" + String.format("%03d", sequenceNumber), currentFillColor, tempStart, tempEnd, currentOutlineColor));

			}else if(outlineEnabledFlag && !currentLabelFlag){
				shapes.add(new Outline(new Rectangle("R" + String.format("%03d", sequenceNumber),currentFillColor,tempStart,tempEnd, currentOutlineColor)));
			}else if(currentLabelFlag && !outlineEnabledFlag){
				shapes.add(new Label( new Rectangle("R" + String.format("%03d", sequenceNumber),currentFillColor,tempStart,tempEnd, currentOutlineColor)));
			}else if(outlineEnabledFlag && currentLabelFlag){
				shapes.add(new Label( new Outline(new Rectangle("R" + String.format("%03d", sequenceNumber),currentFillColor, tempStart, tempEnd, currentOutlineColor))));
			}	
		}
		else if (currentShape == Shape.ShapeType.Ellipse){
			if(!outlineEnabledFlag && !currentLabelFlag){
				shapes.add(new Ellipse("E" + String.format("%03d", sequenceNumber), currentFillColor, tempStart, tempEnd, currentOutlineColor));
			}else if(outlineEnabledFlag && !currentLabelFlag){
				shapes.add(new Outline(new Ellipse("E" + String.format("%03d", sequenceNumber),currentFillColor,tempStart,tempEnd, currentOutlineColor)));
			}else if(currentLabelFlag && !outlineEnabledFlag){
				shapes.add(new Label(new Ellipse("E" + String.format("%03d", sequenceNumber),currentFillColor,tempStart,tempEnd, currentOutlineColor)));
			}else if(outlineEnabledFlag && currentLabelFlag){
				shapes.add(new Label(new Outline( new Ellipse("E" + String.format("%03d", sequenceNumber),currentFillColor, tempStart, tempEnd, currentOutlineColor))));
			}
		}
		else if(currentShape == Shape.ShapeType.Triangle){
			if(!outlineEnabledFlag && !currentLabelFlag){
				shapes.add(new Triangle("T" + String.format("%03d", sequenceNumber), currentFillColor, tempStart, tempEnd, currentOutlineColor));
			}else if(outlineEnabledFlag && !currentLabelFlag){
				shapes.add(new Outline(new Triangle("T" + String.format("%03d", sequenceNumber),currentFillColor,tempStart,tempEnd, currentOutlineColor)));
			}else if(currentLabelFlag && !outlineEnabledFlag){
				shapes.add(new Label(new Triangle("T" + String.format("%03d", sequenceNumber),currentFillColor,tempStart,tempEnd, currentOutlineColor)));
			}else if(outlineEnabledFlag && currentLabelFlag){
				shapes.add(new Label(new Outline( new Triangle("T" + String.format("%03d", sequenceNumber),currentFillColor, tempStart, tempEnd, currentOutlineColor))));
			}
		}
		sequenceNumber++;
		notifyObservers();
		return shapes.get(shapes.size()-1);
	}

	/**
	 * method creates a Composite shape, removes the two shapes from the list, and adds the composite back into the list
	 * @param shapesToCompose
	 * @author khodls
	 */
	public CompositeShape compose(List<Shape> shapesToCompose){
	
		double x = Math.min(shapesToCompose.get(0).getStart().getX(), shapesToCompose.get(0).getEnd().getX());
		double y = Math.min(shapesToCompose.get(0).getStart().getY(), shapesToCompose.get(0).getEnd().getY());
		double x2 = Math.max(shapesToCompose.get(1).getEnd().getX(), shapesToCompose.get(1).getStart().getX());
		double y2 = Math.max(shapesToCompose.get(1).getEnd().getY(), shapesToCompose.get(1).getStart().getY());
		String name = "[";
		for(int i = 0; i<shapesToCompose.size(); i++){
			if(i==shapesToCompose.size()-1){
				name = name + shapesToCompose.get(i).getName();
			}
			else
				name = name + shapesToCompose.get(i).getName() + ":";
			
			Point temp1 = shapesToCompose.get(i).getStart();
			Point temp2 = shapesToCompose.get(i).getEnd();
			x = Math.min(Math.min(temp1.getX(), x), Math.min(temp2.getX(), x2));
			y = Math.min(Math.min(temp1.getY(), y), Math.min(temp2.getY(), y2));
			x2 = Math.max(Math.max(temp1.getX(), x2), Math.max(temp2.getX(), x));
			y2 = Math.max(Math.max(temp1.getY(), y), Math.max(temp2.getY(), y2));
		}
		Point p1 = new Point((int)x, (int)y);
		Point p2 = new Point ((int)x2,(int)y2);
		CompositeShape com = new CompositeShape(name + "]" , Color.GREEN,p1, p2, Color.BLUE );//TODO I edited the names of the shapes. This should just group them in a bracket and add a colon inside them
		com.add(shapesToCompose);
		for(int i =0; i<shapesToCompose.size(); i++){
			shapes.remove(shapesToCompose.get(i));
		}
		shapes.add(com);
		notifyObservers();
		return com;
	}

	/**
	 * gets the children of the composite shape being decomposed, removes the composite shape from the list, and adds in the children.
	 * @param shape
	 * @author khodls, modified by minorr
	 */
	public CompositeShape decompose(Shape shape){
		Shape rmshape = null;
		int i = -1;
		
		while(rmshape == null){
			i++;
			
			if(shapes.get(i).getName().equals(shape.getName())){//If these are the same
				rmshape = shapes.get(i);
			}//else keep looking
		}
		
		if(rmshape.isComposite()){
			ArrayList temp = ((CompositeShape) rmshape).getChild();
			shapes.addAll(i, temp);
			shapes.remove(rmshape);
			notifyObservers();
			return (CompositeShape) rmshape;
		}else{
			return null;
			
		}
	}

	/**
	 * @return the currentFillColor
	 */
	public Color getCurrentFillColor() {
		return currentFillColor;
	}

	/**
	 * @return the currentOutlineColor
	 */
	public Color getCurrentOutlineColor() {
		return currentOutlineColor;
	}

	/**
	 * @return the currentShape
	 */
	public AbstractShape.ShapeType getCurrentShape() {
		return currentShape;
	}

	/**
	 * Iterate over & draw shapes
	 *
	 * @param g
	 *            -- Target graphics object
	 */
	public void draw(Graphics g) {
		for(int i = 0; i<shapes.size(); i++){
			shapes.get(i).draw(g);
		}
	}

	/**
	 * @return the currentLabelFlag
	 */
	public boolean isCurrentLabelFlag() {
		return currentLabelFlag;
	}

	/**
	 * @return true if drawing outlined shapes is enabled.
	 */
	public boolean isOutlineEnabled() {
		return outlineEnabledFlag;
	}

	/**
	 * Remove a shape from the collection
	 * 
	 * @param shape
	 *            the Shape to be removed, including its desctempEndants
	 */
	public void removeShape(Shape shape) {
		shapes.remove(shape);
		notifyObservers();
	}

	/**
	 * 
	 * @param currentFillColor
	 *            the currentFillColor to set
	 */
	public void setCurrentFillColor(Color currentFillColor) {
		this.currentFillColor = currentFillColor;
	}

	/**
	 * 
	 * @param currentLabelFlag
	 *            the currentLabelFlag to set
	 */
	public void setCurrentLabelFlag(boolean currentLabelFlag) {
		this.currentLabelFlag = currentLabelFlag;
	}

	/**
	 *
	 * @param outlineEnabledFlag
	 *            if true, enable drawing outlines.
	 */
	public void setOutlineEnabledFlag(boolean outlineEnabledFlag) {
		this.outlineEnabledFlag = outlineEnabledFlag;
	}

	/**
	 * 
	 * @param currentOutlineColor
	 *            the currentOutlineColor to set
	 */
	public void setCurrentOutlineColor(Color currentOutlineColor) {
		this.currentOutlineColor = currentOutlineColor;
	}

	/**
	 * @param currentShape
	 *            the currentShape to set EXTRA CREDIT TODO: Eliminate ShapeType
	 *            enum and use a FactoryPattern to handle this!
	 */
	public void setCurrentShape(Shape.ShapeType currentShape) {
		System.out.println("changing currentShape to "+ currentShape);
		this.currentShape = currentShape;
	}
	
	/**
	 * @see java.util.Observable#addObserver(java.util.Observer)
	 * @author minorr
	 */
	@Override
	public synchronized void addObserver(java.util.Observer o) {
		ourUIObservers.add(o);
	}

	/**
	 * @see java.util.Observable#notifyObservers()
	 * @author minorr
	 */
	@Override
	public void notifyObservers() {
		for(java.util.Observer observer : ourUIObservers){
			observer.update(this, null);
		}
	}
	
	/**
	 * @return the shapes
	 * @author minorr
	 */
	public List<Shape> getShapes() {
		return shapes;
	}
}