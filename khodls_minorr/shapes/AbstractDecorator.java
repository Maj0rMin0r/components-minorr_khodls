package khodls_minorr.shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

/**
 * This class is a simple class that all decorators implement
 * @author schaba and khodls
 * @version 1.0
 */
public abstract class AbstractDecorator implements Shape {
	//Shape object that is visible to all of the child classes
	protected Shape shape;

	/**
	 * constructor that allows child classes access to the passed in Shape object
	 * @param shape
	 */
	public AbstractDecorator(Shape shape){
		this.shape = shape;
	}

	/**
	 * draw method needed in all the decorators
	 * @param g
	 */
	public abstract void draw(Graphics g);
		
		
		
	
}
