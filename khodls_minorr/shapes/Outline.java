package khodls_minorr.shapes;

import java.awt.*;

public class Outline extends AbstractDecorator {
	

	/**
	 * constructor that calls the parent methods constructor
	 * @param shape
	 */
	public Outline(Shape shape) {
		super(shape);
	}

	/**
	 * first calls draw on the shape object passed in, and then draws a uses graphics 2d to draw an outline around the shape.
	 * @param g
	 */
	public void draw(Graphics g) {
		shape.draw(g);
		Graphics2D g2 = (Graphics2D) g;
		float thickness = 3;
		Stroke oldStroke = g2.getStroke();
		g2.setStroke(new BasicStroke(thickness));
		int x = (int) Math.min(shape.getStart().getX(), shape.getEnd().getX());
		int y = (int) Math.min(shape.getStart().getY(), shape.getEnd().getY());
		int w = (int) Math.abs(shape.getStart().getX() - shape.getEnd().getX());
		int h = (int) Math.abs(shape.getStart().getY() - shape.getEnd().getY());
		g2.setColor(shape.getOutlineColor());
		if(shape instanceof Rectangle){
		g2.drawRect(x, y, w, h);
		}else if(shape instanceof Ellipse){
			g2.drawOval(x, y, w, h);
		}
		else if(shape instanceof Triangle){
				 int x1 = (int)shape.getStart().getX();
			     int x2 = (int)shape.getEnd().getX();
			     int y1 = (int)shape.getStart().getY();
			     int y2 = y1;
			     int x3 = (int)(shape.getEnd().getX()- (shape.getEnd().getX()-shape.getStart().getX())/2);
			     int y3 =(int)shape.getEnd().getY();
			     g2.drawPolygon(new int[]{x1,x3,x2}, new int[]{y1,y3,y2}, 3);
			}
		g2.setStroke(oldStroke);
	}
	/**
	 * returns the color of the shape
	 * @return
	 */
	public Color getColor() {
		return shape.getColor();
	}

	/**
	 * returns the end point of the shape
	 * @return
	 */
	public Point getEnd() {
		return shape.getEnd();
	}

	/**
	 * returns the name of the shape
	 * @return
	 */
	public String getName() {
		return shape.getName();
	}

	/**
	 * returns the start point of the shape
	 * @return
	 */
	public Point getStart() {
		return shape.getStart();
	}

	/**
	 * returns the outline color of the shape
	 * @return
	 */
	public Color getOutlineColor() {
		return shape.getOutlineColor();
	}

	@Override
	public boolean isComposite() {
		// TODO Auto-generated method stub
		return shape.isComposite();
	}
}


