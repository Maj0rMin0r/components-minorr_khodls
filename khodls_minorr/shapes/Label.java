package khodls_minorr.shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

/**
 * This is a concrete decorator class that draws a label above the shape displaying the shapes name
 */
public class Label extends AbstractDecorator {

	/**
	 * Constructor that calls the parent class's constructor
	 * @param shape
	 */
	public Label(Shape shape) {
		super(shape);
	}

	/**
	 * first calls draw on the shape object passed in, and then draws a label of the name of the shape above the shape.
	 * @param g
	 */
	public void draw(Graphics g) {
		shape.draw(g);
		System.out.println("drawing label...");
		int x = (int) Math.min(shape.getStart().getX(), shape.getEnd().getX());
		int y = (int) Math.min(shape.getStart().getY(), shape.getEnd().getY());
		g.drawString(shape.getName(), x, y);
	}

	/**
	 * returns the color of the shape
	 * @return
	 */
	public Color getColor() {
		return shape.getColor();
	}

	/**
	 * returns the end point of the shape
	 * @return
	 */
	public Point getEnd() {
		return shape.getEnd();
	}

	/**
	 * returns the name of the shape
	 * @return
	 */
	public String getName() {
		return shape.getName();
	}

	/**
	 * returns the start point of the shape
	 * @return
	 */
	public Point getStart() {
		return shape.getStart();
	}

	@Override
	public Color getOutlineColor() {
		return shape.getOutlineColor();
	}

	@Override
	public boolean isComposite() {
		// TODO Auto-generated method stub
		return shape.isComposite();
	}
}
