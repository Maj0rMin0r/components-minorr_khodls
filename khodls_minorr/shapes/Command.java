package khodls_minorr.shapes;

public interface Command {

		/**
		 * executes a command
		 */
		public void execute();
		
		/**
		 * reverses the command that execute did
		 */
		public void unexecute();
		
}
