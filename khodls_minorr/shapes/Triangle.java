package khodls_minorr.shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

/**
 * This class represents a Triangle shape that can draw itself to a graphics
 * context
 * @author khodls
 */
public class Triangle extends AbstractShape {
	/**
	 * Constructor for initializing the attributes of a Triangle
	 * 
	 * @param name    identifier for this shape
	 * @param clr    pen color to use when drawing this shape
	 * @param p1    starting (upper left) coordinate for this shape
	 * @param p2    ending (lower right) coordinate for this shape
	 */
	public Triangle(String name, Color clr, Point p1, Point p2, Color outlineColor){
        super(name, clr, p1, p2, outlineColor);
	}

	/**
	 * This method implements the Rectangle-specific version of the draw() method
	 * defined within the abstract Shape class
	 * 
	 * @param g    - the Graphics context to use for drawing
	 */
	public void draw(Graphics g){
		   	System.out.println("DEBUG: Triangle.draw: We are trying to draw a Triangle.");
	        g.setColor(color);
	        System.out.println("drawing a triangle");
	        int x1 = (int)start.getX();
	        int x2 = (int)end.getX();
	        int y1 = (int)start.getY();
	        int y2 = y1;
	        int x3 = (int)(end.getX()- (end.getX()-start.getX())/2);
	        int y3 =(int)end.getY();
	        g.fillPolygon(new int[]{x1,x3,x2}, new int[]{y1,y3,y2}, 3);
	      
	}

	/**
	 * used to get the color if an outline is desired
	 */
	@Override
	public Color getOutlineColor() {
		// TODO Auto-generated method stub
		return outlineColor;
	}

}