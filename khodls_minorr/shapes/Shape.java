package khodls_minorr.shapes;

import java.awt.Graphics;
import java.awt.Point;

/**
 * @author hornick
 * @version 1.0
 * @created 21-Jan-2014 6:13:49 PM
 */
public interface Shape {

	/**
	 * @author hornick
	 * @version 1.0
	 * @created 21-Jan-2014 6:13:49 PM
	 */
	public enum ShapeType {
		/**
		 * the type of basic shapes that can be created
		 */
		Rectangle,
		Ellipse,
		Triangle
	}

	/**
	 * 
	 * @param g
	 */
	public abstract void draw(Graphics g);

	public abstract java.awt.Color getColor();

	public abstract Point getEnd();

	public abstract String getName();

	public abstract Point getStart();
	
	public abstract java.awt.Color getOutlineColor();
	
	public abstract boolean isComposite();

}