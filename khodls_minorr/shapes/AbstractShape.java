package khodls_minorr.shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

/**
 * An AbstractShape represents an element to be displayed in a window.
 * AbstractShape-derived objects are stored in a collection maintained by the
 * ShapeManager class.
 * @author hornick
 * @version 1.0
 * @created 21-Jan-2014 6:13:48 PM
 */
public abstract class AbstractShape implements Shape {

	/**
	 * color to be used to draw this shape
	 */
	protected Color color;
	/**
	 * shape name
	 */
	protected String name;
	/**
	 * starting coordinate (one corner of bounding box)
	 */
	protected Point start;
    /**
     * ending coordinate (opposite corner of bounding box from start)
     */
    protected Point end;
    
    /**
     * Color for the outline
     */
    protected Color outlineColor;
    

	/**
	 * Constructor for initializing the common attributes of a shape
	 *
	 * @param name    identifier for this shape
	 * @param clr    pen color to use when drawing this shape
	 * @param p1    starting (upper left) coordinate for this shape
	 * @param p2    ending (lower right) coordinate for this shape
	 */
	public AbstractShape(String name, Color clr, Point p1, Point p2, Color outlineColor){
        start = p1;
        end = p2;
        color = clr;
        this.name = name;
        this.outlineColor = outlineColor;
	}

	/**
	 * This abstract method defines the draw() method interface that every concrete
	 * shape class must implement
	 *
	 * @param g    - the Graphics context to use for drawing
	 */
	@Override
	public abstract void draw(Graphics g);

	/**
	 * @return the color
	 */
	@Override
	public Color getColor(){
		return color;
	}

	/**
	 * @return the end
	 */
	@Override
	public Point getEnd(){
		return end;
	}

	/**
	 * @return the name
	 */
	@Override
	public String getName(){
		return name;
	}
	
	/**
	 * We'll just say this is false unless we override this method
	 * @author minorr
	 */
	@Override
	public boolean isComposite(){
		return false;
	}
	
	/**
	 * @return the start
	 */
	@Override
	public Point getStart(){
		return start;
	}

	@Override
	public String toString(){
		return "Shape: "+name+" start:"+start+" end:"+end;
	}
}