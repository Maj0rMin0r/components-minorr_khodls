
package khodls_minorr.shapes;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Stroke;
import java.util.ArrayList;
import java.util.List;

/**
 * This class represents an Composite shape that can draw itself to a graphics
 * context
 * @author khodls
 */
public class CompositeShape extends AbstractShape {
	/**
	 * the list containing the objects in the composite shape(other shape objects whether that be composite or otherwise)
	 */
	ArrayList<Shape> shapes = new ArrayList<>();
	
	public CompositeShape(String name, Color clr, Point p1, Point p2, Color outlineColor) {
		super(name, clr, p1, p2, outlineColor);
	}
	
	/**
	 * add the list of shapes to the composite object
	 * @param addShapes
	 */
	public void add(List<Shape> addShapes){
		for(int i = 0; i<addShapes.size(); i++){
			shapes.add(addShapes.get(i));
		}
	}
	
	/**
	 * remove the specified shape from the composite object
	 * @param shape
	 */
	public void remove(Shape shape){
		shapes.remove(shape);
	}
	
	/**
	 * returns all of the shapes that make up the composite object
	 * @return
	 */
	public ArrayList<Shape> getChild(){
		return shapes;
	}
	
	/**
	 * draws the outline of the composite
	 */
	public void draw(Graphics g){
		
		for(int i =0; i<shapes.size(); i++){
			shapes.get(i).draw(g);
		}
		int x = (int) Math.min(start.getX(), end.getX());
		int y = (int) Math.min(start.getY(), end.getY());
		int w = (int) Math.abs(start.getX() - end.getX());
		int h = (int) Math.abs(start.getY() - end.getY());
		Graphics2D g2 = (Graphics2D) g;
		float thickness = 3;
		Stroke oldStroke = g2.getStroke();
		g2.setStroke(new BasicStroke(thickness));
		g2.drawRect(x, y, w, h);
		g2.setStroke(oldStroke);
	}

	@Override
	public Color getOutlineColor() {
		return outlineColor;
	}

	@Override
	public boolean isComposite() {
		boolean isComposite;
		if(this.shapes.size()>1){
			isComposite = true;
		}else{
			isComposite = false;
		}
		return isComposite;
	}
}