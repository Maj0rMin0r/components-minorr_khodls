package khodls_minorr.shapes;

import java.util.Stack;

/**
 * holds a alist of commands that have been done 
 * @author khodls
 *
 */
public class CommandInvoker {

	/**
	 * stack that holds commands for undoing
	 */
	private Stack<Command> cmds = new Stack<Command>();
	
	/**
	 * stack that holds commands for redoing
	 */
	private Stack<Command> redo = new Stack<Command>();

	private static CommandInvoker cmdInv;

	/**
	 * makes sure that only one instance of CommandInvoker is used(Singleton)
	 * @return
	 */
	public static CommandInvoker getInstance(){
		if(cmdInv == null){
			synchronized(CommandInvoker.class){
				if(cmdInv == null){
					cmdInv = new CommandInvoker();
				}
			}
		}
		return cmdInv;
	}
	
	/**
	 * executes the given command and saves it on the stack
	 * @param cmd
	 */
	public void invokeCommand(Command cmd){
		cmds.push(cmd);
		System.out.println("placing a command on the stack" + cmd.toString());
		cmd.execute();
	}
	
	/**
	 * gets the last command that was executed off the stack and then un-executes it
	 */
	public void undo(){
		if(cmds.size()>0){
			Command cmd  = cmds.pop();
			cmd.unexecute();
			redo.push(cmd);
			System.out.println("undoing a cmd"+cmd.toString());
		}
		else
			System.out.println("couldn't undo because no commands left");
	}
	
	/**
	 * 
	 */
	public void redo(){
		if(redo.size()>0){
			Command cmd = redo.pop();
			cmds.push(cmd);
			cmd.execute();
		}
		else
			System.out.println("couldnt redo because nothing on stack");
	
	}

}
